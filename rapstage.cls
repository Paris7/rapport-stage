\ProvidesClass{rapstage}[2024/02/06 Rapport de stage de Paris Cité]

\LoadClass[12pt]{article}

% Options
\newif\ifoption@code
\DeclareOption{code}{\option@codetrue}
\newif\ifoption@f
\DeclareOption{f}{\option@ftrue}
\newif\ifoption@n
\DeclareOption{n}{\option@ntrue}

\newcommand\@student{Étudiant}

\ProcessOptions
\ifoption@code
% Intégration code
\RequirePackage{minted}
\usemintedstyle{emacs}
\fi
\ifoption@f
% Mode féminin
\renewcommand\@student{Étudiante}
\fi
\ifoption@n
% Mode neutre
\renewcommand\@student{Étudiant·e}
\fi

% Encodage
\RequirePackage[T1]{fontenc}

% Polices
\RequirePackage[nopatch=footnote]{microtype}
\RequirePackage{fontspec}
\setmainfont{Times New Roman}

% Langage
\RequirePackage[french]{babel}
\frenchsetup{SmallCapsFigTabCaptions=false}

% Guillemets
\RequirePackage[style=english]{csquotes}
\MakeOuterQuote{"}

% Format des pages
\RequirePackage[a4paper,top=35mm,bottom=33mm,left=5cm,right=5cm]{geometry}

% Style de bibliographie
\newcommand\@bib{Bibliographie}
\renewcommand\frenchrefname{\hspace{4em}\@bib}
\bibliographystyle{unsrt}

% Auteur
\newcommand\me@unstarred[3]{
  \gdef\@mef{#1}         % first name
  \gdef\@mel{#2}         % last name
  \gdef\@mail{mailto:#3} % mail
}
\newcommand\me@starred[2]{
  \gdef\@mef{#1}         % first name
  \gdef\@mel{#2}         % last name
  \gdef\@mail{}          % mail
}
\newcommand\me{\@ifstar{\me@starred}{\me@unstarred}}
\newcommand\@mef{Alice}
\newcommand\@mel{Dubois}
\newcommand\@mail{mailto:alice.dubois@example.com}

% Titre
\renewcommand\title[1]{\gdef\@title{#1}}
\renewcommand\@title{TITRE DU RAPPORT}

% Date
\newcount\lastyear
\lastyear=\year
\advance\lastyear by -1

\renewcommand\date[1]{\gdef\@date{#1}}
\renewcommand\@date{\the\lastyear{}/\the\year{}}

% Tuteur pédagogique
\newcommand\tuteurpedago@unstarred[2]{
  \gdef\@tpn{#1}        % nom
  \gdef\@tpm{mailto:#2} % mail
}
\newcommand\tuteurpedago@starred[1]{
  \gdef\@tpn{#1}        % nom
  \gdef\@tpm{}          % mail
}
\newcommand\tuteurpedago{
\@ifstar{\tuteurpedago@starred}{\tuteurpedago@unstarred}}
\newcommand\@tpn{John \textsc{Doe}}
\newcommand\@tpm{mailto:john.doe@example.com}

% Tuteur entreprise
\newcommand\tuteurentreprise@unstarred[2]{
  \gdef\@ten{#1}        % nom
  \gdef\@tem{mailto:#2} % mail
}
\newcommand\tuteurentreprise@starred[1]{
  \gdef\@ten{#1}        % nom
  \gdef\@tem{}          % mail
}
\newcommand\tuteurentreprise{
\@ifstar{\tuteurentreprise@starred}{\tuteurentreprise@unstarred}}
\newcommand\@ten{Bob \textsc{Dupont}}
\newcommand\@tem{mailto:bob.dupont@example.com}

% Directeurice université
\newcommand\directeur[1]{\gdef\@directeur{#1}}
\newcommand\@directeur{Carole \textsc{Delporte}}

% Sous-titre
\newcommand\subtitle[1]{\gdef\@subtitle{#1}}
\newcommand\@subtitle{Rapport de Stage de Master 2}

% Fichier de la bibliographie
\newcommand\bibliofile[1]{\gdef\@refile{#1}}

% Mise en page
\RequirePackage{titlesec}
\titleformat*{\section}{\fontspec{Arial}\Large\bfseries}
\titleformat*{\subsection}{\fontspec{Arial}\large\bfseries\itshape}

% Point après numéro dans le titre
\RequirePackage{titlesec}
\titlelabel{\thetitle.\hspace{0.8em}}

% Interligne
\RequirePackage{setspace}
\setstretch{1.15}

% Page current/max
\RequirePackage[page]{totalcount}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\cfoot{\makebox[\textwidth][c]{\thepage/\totalpages}}
\renewcommand\headrulewidth{0pt}

% Liens cliquable et métadonnées
\RequirePackage{hyperref}
\AtBeginDocument{
  \hypersetup{
    pdfauthor={\@mef~\@mel},
    pdftitle={\@title},
    pdfkeywords={rapport, stage},
    pdfsubject={Rapport de stage pour l'Université Paris Cité},
    pdfcreator={LaTeX with rapstage class:
    https://moule.informatique.univ-paris-diderot.fr/kennel/rapport-stage},
    hidelinks,
  }
}

% Images
\RequirePackage{graphicx,float}

% Figures
\RequirePackage[font=it,labelsep=colon]{caption}
\renewcommand\frenchfigurename{Illustration}
\addto\extrasfrench{\def\figureautorefname{illustration}}
\AtBeginDocument{
  \renewcommand{\listfigurename}{Table des illustrations}
}

% Listes
\AtBeginDocument{\def\labelitemi{\scriptsize$\bullet$}
  \def\labelitemii{\scriptsize$\bullet$}
  \def\labelitemiii{\scriptsize$\bullet$}
  \def\labelitemiv{\scriptsize$\bullet$}
}

% Souligne mieux les mots
\RequirePackage{ulem}
\setlength{\ULdepth}{2pt}
\renewcommand\ULthickness{.9pt}

\renewcommand\maketitle{
  \begin{titlepage}
    \begin{center}
      \includegraphics[alt={Logo},height=27mm]{images/logo.png}

      {
        \vspace{4mm}
        \fontspec{Arial}
        \Large
        Université Paris Cité \\
      }

      \vspace{11mm}

      \large
      \textsc{Faculté de Sciences} \\

      \vspace{1mm}
      UFR d'Informatique \\

      \vspace{-2mm}
      Directrice : \@directeur \\

      \vspace{14mm}
      \Large
      \textsc{\@subtitle} \\

      \vspace{3mm}
      \LARGE
      \textsc{\textbf{\uline{\@title}}}
    \end{center}

    \vfill

    \large
    \noindent
    {
      \setstretch{0.9}
      \begin{tabular}{ll}
        \itshape
        Tuteur pédagogique :   & \itshape\href{\@tpm}{\@tpn} \\

        \itshape
        Tuteur en entreprise : & \itshape\href{\@tem}{\@ten} \\
      \end{tabular}
    }

    \vspace{19mm}

    \hfill
    \textit{\@student : \href{\@mail}{\@mef~\textsc{\@mel}}}

    \vspace{17mm}
    \Large
    \centering\textsc{Année universitaire \@date}
  \end{titlepage}

  % Nouvelle page vierge
  \setcounter{page}{3}
  \shipout\null

  \newgeometry{right=2cm,left=4cm,bottom=46mm}
}

% Permet d'ajouter la bibliographie
\RequirePackage{etoolbox}
\newcommand\@reference{
  \newpage
  \phantomsection
  \addcontentsline{toc}{section}{\@bib}
  \bibliography{\@refile}

  \newpage
  \pagestyle{empty}
}
\newcommand\reference@unstarred{\@reference}
\newcommand\reference@starred[1]{
  \patchcmd{\thebibliography}{\list}{#1 \\ \list}{}{}
  \@reference
}
\newcommand\reference{\@ifstar{\reference@starred}{\reference@unstarred}}

% Annexe
\newcommand\@annexe{Annexe}
\newcommand\annexe{
  \newpage
  \appendix
  \phantomsection
  \addcontentsline{toc}{section}{\@annexe}
  \section*{\hspace{4em}\@annexe}
}

% Ne déborde pas sur la marge
\AtBeginDocument{
  \sloppy
}
