# Rapport de stage

Classe [`rapstage`](./rapstage.cls) pour faire un rapport de stage à Paris Cité.

## Prérequis

- [`latexmk`](https://wiki.archlinux.org/title/TeX_Live)
- [Police Windows pour `Arial` et `Times New Roman`](https://aur.archlinux.org/packages/ttf-ms-win10-cdn)

## Options

| Option | Utilité                 |
| :----: | :---------------------- |
| `code` | Utilisation de `minted` |
|  `f`   | Pour les étudiantes     |
|  `n`   | Pour les étudiant·e·s   |

## Commandes

|                                Commande | Utilité                                                    |
| --------------------------------------: | :--------------------------------------------------------- |
|              `\me{prénom}{nom}{mail}`\* | Défini l'identité de l'étudiant·e                          |
|                   `\me*{prénom}{nom}`\* | Défini l'identité de l'étudiant·e **sans mail**            |
|                       `\title{titre}`\* | Défini le titre du rapport                                 |
|                    `\date{AAAA/AAAA}`\* | Défini l'année universitaire                               |
|     `\tuteurpedago{prénom nom}{mail}`\* | Défini l'identité du tuteurice pédagogique **sans mail**   |
|          `\tuteurpedago*{prénom nom}`\* | Défini l'identité du tuteurice pédagogique                 |
| `\tuteurentreprise{prénom nom}{mail}`\* | Défini l'identité du tuteurice en entreprise               |
|      `\tuteurentreprise*{prénom nom}`\* | Défini l'identité du tuteurice en entreprise **sans mail** |
|              `\directeur{prénom nom}`\* | Défini l'identité du directeurice                          |
|               `\subtitle{sous titre}`\* | Défini le sous-titre du rapport                            |
|                 `\bibliofile{chemin}`\* | Défini le chemin vers la bibliographie                     |
|                            `\reference` | Affiche la bibliographie                                   |
|                  `\reference*{préface}` | Affiche la bibliographie **avec une préface**              |
|                               `\annexe` | Commence l'annexe                                          |

> \* Commande possédant une valeur par défaut.

## Exemple

Le modèle [disponible ici](https://www.informatique.univ-paris-diderot.fr/_media/formations/masters/template_stage_m2.pdf)
est retranscrit en LaTeX dans le dossier [modele/](./modele)

- Téléchargez/mettez à jour la classe

  ```bash
  make updateclass
  ```

- Compiler le modèle

  ```bash
  make
  ```

  Ce qui va générer un `rapport.pdf`.
